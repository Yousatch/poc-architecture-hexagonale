package configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import adapter.ClientServiceImpl;
import ports.ClientIdRepository;
import ports.ClientRepository;
import ports.ClientService;
import ports.SituationClientRepository;

@Configuration
public class ApplicationConfiguration {

	@Bean
	public ClientService clientService(ClientRepository clientRepository,
			ClientIdRepository clientIdRepository,
			SituationClientRepository situationClientRepository) {
		return new ClientServiceImpl(clientRepository, clientIdRepository, situationClientRepository);
	}


	// @Bean
	// public ClientRepository clientRepository() {
	// return new ClientRepositoryJpaAdapter();
	// }
	//
}