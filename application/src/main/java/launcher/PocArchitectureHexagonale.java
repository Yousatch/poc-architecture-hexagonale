package launcher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocArchitectureHexagonale {

	public static void main(String[] args) {
		SpringApplication.run(PocArchitectureHexagonale.class, args);
	}
}