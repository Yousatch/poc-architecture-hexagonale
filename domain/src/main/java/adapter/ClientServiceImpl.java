package adapter;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import models.entities.Client;
import models.valueObject.ClientId;
import models.valueObject.SituationClient;
import ports.ClientIdRepository;
import ports.ClientRepository;
import ports.ClientService;
import ports.SituationClientRepository;

public class ClientServiceImpl implements ClientService {

	private ClientRepository clientRepository;

	private ClientIdRepository clientIdRepository;

	private SituationClientRepository situationClientRepository;

	public ClientServiceImpl(ClientRepository clientRepository,
			ClientIdRepository clientIdRepository,
			SituationClientRepository situationClientRepository) {
		this.clientRepository = clientRepository;
		this.clientIdRepository = clientIdRepository;
		this.situationClientRepository = situationClientRepository;
	}

	@Override
	public String generateMatricule() {
		return MatriculeGenerator.generate("18", 8);
	}

	@Override
	public void addClient(Client client) {
		ClientId clientId = client.getClientId();
		clientId.setMatricule(generateMatricule());
		if (StringUtils.isBlank(client.getClientId().getRang())) {
			clientId.setRang(String.valueOf(0));
		}
		clientIdRepository.addClientId(clientId);
		client.setClientId(clientId);
		clientRepository.addClient(client);
	}

	@Override
	public List<Client> getClients() {
		return clientRepository.getClients();
	}

	@Override
	public void deleteClient(ClientId clientId) {
		clientIdRepository.deleteClientId(clientId);
		clientRepository.deleteClient(clientId);
	}

	@Override
	public Optional<Client> getClient(String matricule, String rang) {
		Optional<ClientId> clientId = clientIdRepository.getClientIdByMatriculeAndRang(matricule, rang);
		if (!clientId.isPresent()) {
			try {
				throw new Exception("clientId not found : " + matricule + " " + rang);
			} catch (Exception e) {
				return Optional.empty();
			}
		}
		return clientRepository.getClient(clientId.get());
	}

	@Override
	public void updateClient(Client client) {
		clientRepository.updateClient(client);
	}

	@Override
	public void changerSituation(String matricule, String rang, String situation) {
		Optional<Client> clientOptional = getClient(matricule, rang);
		if (!clientOptional.isPresent()) {
			try {
				throw new Exception("client not found : " + matricule + " " + rang);
			} catch (Exception e) {
				return;
			}
		}
		Client client = clientOptional.get();
		Optional<SituationClient> situationClient = situationClientRepository.getBySituation(situation);
		if (!situationClient.isPresent()) {
			try {
				throw new Exception("situationClient not found : " + situation);
			} catch (Exception e) {
				return;
			}
		}
		client.setSituationClient(situationClient.get());
		clientRepository.updateClient(client);
	}

}
