package models.entities;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import models.valueObject.Adresse;
import models.valueObject.ClientId;
import models.valueObject.SituationClient;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
@EqualsAndHashCode
public class Client implements Serializable  {
	
	private static final long serialVersionUID = -2342192281610324482L;

	private ClientId clientId;
	
	private String nom;
	
	private String prenom;
	
	private LocalDate dateNaissance;
	
	private String cin;
	
	private String rib;
	
	private SituationClient situationClient;
	
	private Adresse adresse; 

	

	
	
}