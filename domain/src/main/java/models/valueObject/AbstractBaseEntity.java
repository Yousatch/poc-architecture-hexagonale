package models.valueObject;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode(of = "id")
@ToString(of = "id")
public abstract class AbstractBaseEntity implements Serializable {

	private static final long serialVersionUID = 8559905569074982277L;

	public static final String ID_COLUMN_NAME = "id";

	@Getter
	private final long id;

	protected AbstractBaseEntity() {
		id = 0;
	}

	public AbstractBaseEntity(long id) {
		this.id = id;
	}

}
