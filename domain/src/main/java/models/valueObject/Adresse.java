package models.valueObject;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Adresse extends AbstractBaseEntity {

	private static final long serialVersionUID = -3821971229758700231L;
	
	private String Pays;
	
	private String Localite;
	
	private String CodePostal;
	
	private String rue;

}
