package models.valueObject;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ClientId extends AbstractBaseEntity{

	private static final long serialVersionUID = -7385462006070406666L;
	
	private String matricule;
	
	private String rang;


}
