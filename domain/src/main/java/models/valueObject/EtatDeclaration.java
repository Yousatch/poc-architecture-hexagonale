package models.valueObject;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class EtatDeclaration extends AbstractBaseEntity {
	
	private static final long serialVersionUID = 5711694880018649858L;
	
	private boolean isVerified;
	
	private boolean isValid;
	
	private RaisonRejet raisonRejet;
	
}
