package models.valueObject;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class RaisonRejet extends AbstractBaseEntity {

	private static final long serialVersionUID = -4826362763449673529L;
	
	private String raison;

}
