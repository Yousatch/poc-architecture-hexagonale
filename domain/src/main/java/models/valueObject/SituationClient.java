package models.valueObject;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class SituationClient extends AbstractBaseEntity{
	
	private static final long serialVersionUID = -5967522947123442032L;
	
	private String situation;
	
	

}
