package ports;

import java.util.Optional;

import models.valueObject.ClientId;

public interface ClientIdRepository {

	void addClientId(ClientId clientId);

	void deleteClientId(ClientId clientId);

	Optional<ClientId> getClientId(long id);

	Optional<ClientId> getClientIdByMatriculeAndRang(String matricule, String rang);

}
