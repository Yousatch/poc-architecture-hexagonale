package ports;

import java.util.List;
import java.util.Optional;

import models.entities.Client;
import models.valueObject.ClientId;

public interface ClientRepository {
	
	void addClient(Client client);

    List<Client> getClients();

	void deleteClient(ClientId clientId);

	Optional<Client> getClient(ClientId clientId);
    
    void updateClient(Client client);


}
