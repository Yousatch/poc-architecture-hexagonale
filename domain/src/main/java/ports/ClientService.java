package ports;

import java.util.List;
import java.util.Optional;

import models.entities.Client;
import models.valueObject.ClientId;

public interface ClientService {

	String generateMatricule();

	void addClient(Client client);

	List<Client> getClients();

	void deleteClient(ClientId clientId);

	Optional<Client> getClient(String matricule, String rang);

	void updateClient(Client client);

	void changerSituation(String matricule, String rang, String Situation);

}
