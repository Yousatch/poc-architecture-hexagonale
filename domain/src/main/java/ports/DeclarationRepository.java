package ports;

import java.util.List;
import java.util.Optional;

import models.entities.Declaration;

public interface DeclarationRepository {

	void addDeclaration(Declaration declaration);
    
    List<Declaration> getDeclarationsByMatricule(String matriculeCLient);

	void deleteDeclaration(long id);

	Optional<Declaration> getDeclaration(long id);
    
    void updateDeclaration(Declaration declaration);

    
    
}
