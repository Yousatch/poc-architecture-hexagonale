package ports;

import java.util.Optional;

import models.valueObject.DeclarationId;

public interface DeclarationRepositoryId {

	void addDeclarationId(DeclarationId clientId);

	void deleteDeclarationId(DeclarationId clientId);

	Optional<DeclarationId> getDeclarationId(long id);


}
