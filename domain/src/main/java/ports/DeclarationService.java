package ports;

import java.util.List;
import java.util.Optional;

import models.entities.Declaration;
import models.valueObject.DeclarationId;
import models.valueObject.EtatDeclaration;

public interface DeclarationService {
	
	void addDeclaration(Declaration declaration);
    
    List<Declaration> getDeclarationsByMatricule(String matriculeCLient);
    
    List<Declaration> getDeclarationsByMatriculeAndEtat(String matriculeCLient,EtatDeclaration etatDeclaration);

    void deleteDeclaration(DeclarationId declarationId);

    Optional<Declaration> getDeclaration(DeclarationId declarationId);
    
    void updateDeclaration(Declaration declaration);
    
    void changerEtatDeclaration(DeclarationId declarationId,EtatDeclaration etatDeclaration);

}
