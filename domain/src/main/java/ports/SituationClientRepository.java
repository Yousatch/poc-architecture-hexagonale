package ports;

import java.util.Optional;

import models.valueObject.SituationClient;

public interface SituationClientRepository {

	Optional<SituationClient> getBySituation(String situation);

}
