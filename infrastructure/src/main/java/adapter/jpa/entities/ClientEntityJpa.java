
package adapter.jpa.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import models.entities.Client;
import models.valueObject.Adresse;
import models.valueObject.ClientId;
import models.valueObject.SituationClient;

@Entity
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
public class ClientEntityJpa extends Client {


	private static final long serialVersionUID = 7911357558237970842L;

	@EmbeddedId
	@Override
	public ClientId getClientId() {
		// TODO Auto-generated method stub
		return super.getClientId();
	}

	@Override
	public Adresse getAdresse() {
		// TODO Auto-generated method stub
		return super.getAdresse();
	}

	@Override
	public SituationClient getSituationClient() {
		// TODO Auto-generated method stub
		return super.getSituationClient();
	}

}
