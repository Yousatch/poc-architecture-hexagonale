package adapter.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import models.valueObject.ClientId;
import ports.ClientIdRepository;


@Repository
public interface ClientIdRepositoryJpa extends ClientIdRepository, JpaRepository<ClientId, Long> {

}
