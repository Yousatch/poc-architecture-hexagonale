package adapter.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import models.entities.Client;
import models.valueObject.ClientId;
import ports.ClientRepository;

@Repository
public interface ClientRepositoryJpa extends ClientRepository, JpaRepository<Client, ClientId> {

}
