package adapter.jpa.valueObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import models.valueObject.ClientId;

@Entity
@Embeddable
public class ClientIdJpa extends ClientId {

	private static final long serialVersionUID = -4919700960618429415L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = ID_COLUMN_NAME)
	@Override
	public long getId() {
		return super.getId();
	}

}
