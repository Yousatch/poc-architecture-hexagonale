package ma.rcar.formation.restapiadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiAdapterApplication.class, args);
	}

}

