package ma.rcar.formation.restapiadapter.adapters;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.rcar.formation.restapiadapter.entities.Client;
import ma.rcar.formation.restapiadapter.ports.IClientServiceRest;
import ma.rcar.formation.restapiadapter.vos.ClientId;

@RestController
public class ClientServiceRestImpl implements IClientServiceRest {

	
	@Override
	@RequestMapping("/addClient")
	public void addClient(Client clientToAdd) {
		// TODO Auto-generated method stub

	}

	@Override
	@RequestMapping("/getClients")
	public List<Client> getClients() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping("/deleteClient")
	public void deleteClient(ClientId clientId) {
		// TODO Auto-generated method stub

	}

	@Override
	@RequestMapping("/getClientByMatriculeRang")
	public Optional<Client> getClient(String matricule, String rang) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping("/changerSituation")
	public void changerSituation(String matricule, String rang, String situation) {
		// TODO Auto-generated method stub

	}

}
