package ma.rcar.formation.restapiadapter.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.rcar.formation.restapiadapter.vos.DeclarationId;
import ma.rcar.formation.restapiadapter.vos.EtatDeclaration;
import ma.rcar.formation.restapiadapter.vos.Trimestre;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@Getter
@Setter
@EqualsAndHashCode
public class Declaration implements Serializable{
	
	private static final long serialVersionUID = 2652640130603648630L;
	
	private DeclarationId declarationId;
	
	private Client client;
	
	private LocalDate dateDeclaration;
	
	private Trimestre trimestre;
	
	private double salaire;
	
	private double cotsiationRg;
	
	private double cotisationRc;
	
	private long dureeActivitee;
	
	private EtatDeclaration etatDeclaration;
	
	private List<Declaration> declarationSources;

	
	
	
	
	

}
