package ma.rcar.formation.restapiadapter.ports;

import java.util.List;
import java.util.Optional;

import ma.rcar.formation.restapiadapter.entities.Client;
import ma.rcar.formation.restapiadapter.vos.ClientId;

public interface IClientServiceRest {

	void addClient(Client clientToAdd);
	List<Client> getClients();
	void deleteClient(ClientId clientId);
	Optional<Client> getClient(String matricule, String rang);
	void changerSituation(String matricule, String rang, String situation) ;
}
