package ma.rcar.formation.restapiadapter.vos;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@Getter
@Setter
@EqualsAndHashCode
public class DeclarationId {
	
	private String matricule;
	
	private String matriculeEmployeur;
	

}
