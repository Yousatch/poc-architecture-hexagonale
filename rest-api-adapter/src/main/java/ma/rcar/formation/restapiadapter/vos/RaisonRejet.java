package ma.rcar.formation.restapiadapter.vos;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@Getter
@Setter
public class RaisonRejet {
	
	private String raison;

}
