package ma.rcar.formation.restapiadapter.vos;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@Getter
@Setter
public class SituationClient {

	private String situation;
	
	

}
