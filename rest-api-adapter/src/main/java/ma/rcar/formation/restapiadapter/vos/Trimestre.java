package ma.rcar.formation.restapiadapter.vos;

public enum Trimestre {
	
	premier,
	deuxieme,
	troisieme,
	quatrieme;

}
